# Todo List
This is a simple Todo list project that is Implemented with React.js and Node.js.
## Client
It uses the Next.js framework, TailwindCSS for styling, and Context API and SWR for managing states.

### Usage
First make `.env` file and put these lines there.
```
NEXT_PUBLIC_BASE_URL=http://localhost:5000
```
Then you can run it with this command.
```
yarn && yarn dev
```

## Server
It uses the Express.js framework and MongoDB for database.

### Usage
First make `.env` file and put these lines there.
```
PORT=5000
DATABASE_URL=mongodb+srv://core:KzeRCzUbbSlIRK7S@cluster0.zsrqj.mongodb.net/todolist?retryWrites=true&w=majority
GET_RANDOM_TASK_API=https://www.boredapi.com/api/activity
```
Then you can run it with this command.
```
yarn && yarn start
```


