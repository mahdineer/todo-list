import { Types } from "mongoose";
import { Request } from "express";

export interface ITask {
  id: Types.ObjectId;
  title: string;
  done: boolean;
}

export type TCreateTaskRequest = Request<
  unknown,
  unknown,
  Pick<ITask, "title">
>;

export type TUpdateTaskRequest = Request<
  unknown,
  unknown,
  Omit<ITask, "title">
>;

export type TDeleteTaskByIDRequest = Request<Pick<ITask, "id">>;
