export interface ITaskTitle {
  title: string;
}

export interface IBoredAPIResponse {
  activity: string;
  type: string;
  participants: number;
  price: number;
  link: string;
  key: string;
  accessibility: number;
}
