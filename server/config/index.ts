import "dotenv/config";

export const PORT = process.env.PORT || 5000;
export const DATABASE_URL = process.env.DATABASE_URL as string;
export const GET_RANDOM_TASK_API = process.env.GET_RANDOM_TASK_API as string;
