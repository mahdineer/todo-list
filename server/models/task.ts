import { Schema, model } from "mongoose";
import { ITask } from "../types/task";

const taskSchema = new Schema<ITask>({
  title: { type: String, required: true },
  done: { type: Boolean, required: true, default: false },
});

export const Task = model<ITask>("task", taskSchema);
