import { Schema, model } from "mongoose";
import { ITaskTitle } from "../types/taskTitle";

const taskTitleSchema = new Schema<ITaskTitle>({
  title: { type: String, required: true, unique: true },
});

export const TaskTitle = model<ITaskTitle>("task_title", taskTitleSchema);
