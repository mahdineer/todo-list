import axios from "axios";
import { Request, Response } from "express";
import { GET_RANDOM_TASK_API } from "../../config";
import { MESSAGES } from "../../constants";
import { TaskTitle } from "../../models/taskTitle";
import { IBoredAPIResponse } from "../../types/taskTitle";

class TaskTitleService {
  // it fetches some task titles from an API and store them in database.
  public async feed(_: Request, res: Response) {
    try {
      for (let i = 0; i < 100; i++) {
        const { data } = await axios.get<IBoredAPIResponse>(
          GET_RANDOM_TASK_API,
        );
        const title = data.activity;
        const isTaskTitleExist = await TaskTitle.findOne({
          title,
        });

        if (!isTaskTitleExist) {
          const taskTitle = new TaskTitle({
            title,
          });
          await taskTitle.save();
        }
      }
      res.status(200).json({ message: MESSAGES.done });
    } catch (error) {
      res.status(500).json(error);
    }
  }
  public async getRandomTitle(_: Request, res: Response) {
    try {
      const [taskTitle] = await TaskTitle.aggregate([{ $sample: { size: 1 } }]);

      res.status(200).json({
        title: taskTitle?.title,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  }
}

export default new TaskTitleService();
