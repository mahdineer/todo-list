import Joi from "joi";
import { ITask } from "../../types/task";

export const createTaskSchema = Joi.object<Pick<ITask, "title">>({
  title: Joi.string().trim().required(),
});

export const getTaskByTitleSchema = Joi.object<Pick<ITask, "title">>({
  title: Joi.string().trim().required(),
});

export const updateTaskSchema = Joi.object<Omit<ITask, "title">>({
  id: Joi.string().hex().length(24).required(),
  done: Joi.boolean().required(),
});

export const deleteTaskByIDSchema = Joi.object<Pick<ITask, "id">>({
  id: Joi.string().hex().length(24).required(),
});
