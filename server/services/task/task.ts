import { Request, Response } from "express";
import { MESSAGES } from "../../constants";
import { Task } from "../../models/task";
import {
  TCreateTaskRequest,
  TDeleteTaskByIDRequest,
  TUpdateTaskRequest,
} from "../../types/task";
import {
  createTaskSchema,
  deleteTaskByIDSchema,
  getTaskByTitleSchema,
  updateTaskSchema,
} from "./validations";

class TaskService {
  public async create(req: TCreateTaskRequest, res: Response) {
    try {
      const { error, value } = createTaskSchema.validate(req.body);

      if (error) {
        return res.status(400).json({ message: error.message });
      }

      if (value) {
        const { title } = value;
        const task = new Task({
          title,
        });
        await task.save();

        res.status(200).json(task);
      }
    } catch (error) {
      res.status(500).json(error);
    }
  }
  public async getAll(_: Request, res: Response) {
    try {
      const tasks = await Task.aggregate([
        {
          $project: {
            _id: 0,
            id: "$_id",
            title: 1,
            done: 1,
          },
        },
      ]);

      res.status(200).json(tasks);
    } catch (error) {
      res.status(500).json(error);
    }
  }
  public async update(req: TUpdateTaskRequest, res: Response) {
    try {
      const { error, value } = updateTaskSchema.validate(req.body);

      if (error) {
        return res.status(400).json({ message: error.message });
      }

      if (value) {
        const { id, done } = value;
        const task = await Task.findByIdAndUpdate(id, { done }, { new: true });

        res.status(200).json(task);
      }
    } catch (error) {
      res.status(500).json(error);
    }
  }
  public async deleteAll(_: Request, res: Response) {
    try {
      const tasks = await Task.deleteMany();

      res.status(200).json(tasks);
    } catch (error) {
      res.status(500).json(error);
    }
  }
  public async deleteByID(req: TDeleteTaskByIDRequest, res: Response) {
    try {
      const { error, value } = deleteTaskByIDSchema.validate(req.params);

      if (error) {
        return res.status(400).json({ message: error.message });
      }

      if (value) {
        const { id } = value;
        const task = await Task.findByIdAndRemove(id);

        if (task) {
          res.status(200).json(task);
        } else {
          res.status(400).json({
            message: MESSAGES.taskNotFound,
          });
        }
      }
    } catch (error) {
      res.status(500).json(error);
    }
  }
}

export default new TaskService();
