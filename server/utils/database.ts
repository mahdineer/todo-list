import mongoose from "mongoose";
import { DATABASE_URL } from "../config";

export function connectDatabase() {
  mongoose
    .connect(DATABASE_URL)
    .then(() => {
      return console.log("Connected to MongoDB");
    })
    .catch(err => {
      return console.log("MongoDB Error", err);
    });
}
