import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import { PORT } from "./config";
import { taskRouter } from "./routes/task";
import { taskTitleRouter } from "./routes/taskTitle";
import { connectDatabase } from "./utils/database";
import { notFound } from "./routes/notFound";
import { handleError } from "./middlewares/errorHandler";

const app = express();

connectDatabase();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", async (_, res) => {
  return res.json("Todo List");
});

app.use(taskRouter);
app.use(taskTitleRouter);
app.use(notFound);
app.use(handleError);

app.listen(PORT, () =>
  console.log(`Example app listening at http://localhost:${PORT}`),
);
