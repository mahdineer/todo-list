import { ErrorRequestHandler } from "express";

const handleError: ErrorRequestHandler = (err, _, res) => {
  return res.status(err.status || 500).json({ message: err.message });
};

export { handleError };
