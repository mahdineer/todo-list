import { Router, Request, Response } from "express";
import { TaskService } from "../../services/task";
import {
  TDeleteTaskByIDRequest,
  TCreateTaskRequest,
  TUpdateTaskRequest,
} from "../../types/task";

export const taskRouter = Router();

taskRouter.post("/tasks", async (req: TCreateTaskRequest, res: Response) => {
  return TaskService.create(req, res);
});

taskRouter.get("/tasks", async (req: Request, res: Response) => {
  return TaskService.getAll(req, res);
});

taskRouter.put("/tasks", async (req: TUpdateTaskRequest, res: Response) => {
  return TaskService.update(req, res);
});

taskRouter.delete("/tasks", async (req: Request, res: Response) => {
  return TaskService.deleteAll(req, res);
});

taskRouter.delete(
  "/tasks/:id",
  async (req: TDeleteTaskByIDRequest, res: Response) => {
    return TaskService.deleteByID(req, res);
  },
);
