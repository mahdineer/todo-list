import { Router, Request, Response } from "express";
import { TaskTitleService } from "../../services/taskTitle";

export const taskTitleRouter = Router();

taskTitleRouter.get("/feed", async (req: Request, res: Response) => {
  return TaskTitleService.feed(req, res);
});

taskTitleRouter.get("/random-title", async (req: Request, res: Response) => {
  return TaskTitleService.getRandomTitle(req, res);
});
