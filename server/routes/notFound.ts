import { Request, Response } from "express";
import { MESSAGES } from "../constants";

export function notFound(_: Request, res: Response) {
  return res.status(404).json({ message: MESSAGES.routeNotFound });
}
