import { Header } from "@components/Header";
import { Tasks } from "@components/Tasks";
import { ModalProvider } from "@contexts/ModalContext";
import styles from "./Dashboard.module.css";
import { useDashboard } from "./useDashboard";

export function Dashboard() {
  const { tasks, isLoading } = useDashboard();

  return (
    <div className={styles.wrapper}>
      <div className={styles.dashboard}>
        <ModalProvider>
          <Header />
        </ModalProvider>
        <Tasks isLoading={isLoading} tasks={tasks} />
      </div>
    </div>
  );
}
