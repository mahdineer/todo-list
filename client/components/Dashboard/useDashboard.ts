import { SWR_KEYS } from "@constants";
import { callGetTasks } from "@api/tasks";
import { useTasksContext } from "contexts/TasksContext";
import { useEffect } from "react";
import useSWR from "swr";

export function useDashboard() {
  const { tasks, setTasks } = useTasksContext();
  const { data, isValidating: isLoading } = useSWR(
    SWR_KEYS.GET_TASKS,
    callGetTasks,
  );

  useEffect(() => {
    if (data) {
      setTasks(data.data);
    }
  }, [data, setTasks, tasks]);

  return {
    tasks,
    isLoading,
  };
}
