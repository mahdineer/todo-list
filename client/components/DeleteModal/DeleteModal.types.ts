export type IDeleteModalProps = {
  title: string;
  description: string;
  onDelete: () => void;
  isOpen: boolean;
  cancelButtonTitle?: string;
  removeButtonTitle?: string;
};
