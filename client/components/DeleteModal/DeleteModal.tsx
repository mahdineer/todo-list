import { useCallback } from "react";
import { Modal } from "../Modal";
import { Button } from "@components/Button";
import { useModalContext } from "@contexts/ModalContext";
import styles from "./DeleteModal.module.css";
import { IDeleteModalProps } from "./DeleteModal.types";
import { TrashIcon } from "@heroicons/react/outline";
import { useTranslation } from "react-i18next";

export function DeleteModal(props: IDeleteModalProps) {
  const { t } = useTranslation("common");
  const {
    isOpen,
    title,
    description,
    onDelete,
    cancelButtonTitle = t("cancel"),
    removeButtonTitle = t("remove"),
  } = props;
  const { closeModal } = useModalContext();

  const handleDelete = useCallback(() => {
    onDelete();
    closeModal();
  }, [closeModal, onDelete]);

  return (
    <Modal isOpen={isOpen}>
      <div className={styles.modal}>
        <div className={styles.iconWrapper}>
          <TrashIcon className={styles.icon} />
        </div>
        <div className={styles.title}>{title}</div>
        <p className={styles.description}>{description}</p>
        <div className={styles.buttons}>
          <Button
            type="button"
            layout="outline"
            onClick={closeModal}
            className={styles.button}
          >
            {cancelButtonTitle}
          </Button>
          <Button
            type="button"
            layout="remove"
            onClick={handleDelete}
            className={styles.button}
          >
            {removeButtonTitle}
          </Button>
        </div>
      </div>
    </Modal>
  );
}
