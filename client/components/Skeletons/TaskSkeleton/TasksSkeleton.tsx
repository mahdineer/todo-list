import styles from "./TasksSkeleton.module.css";
import { v4 as uuid } from "uuid";

export function TasksSkeleton() {
  const items = [...Array(3)];

  return (
    <div className={styles.wrapper}>
      {items.map(() => (
        <div key={uuid()} className={styles.item}>
          <div className={styles.checkbox}></div>
          <div className={styles.title}></div>
        </div>
      ))}
    </div>
  );
}
