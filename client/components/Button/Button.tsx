import classNames from "classnames";
import styles from "./Buttons.module.css";
import type { IButtonProps } from "./Button.types";

export function Button(props: IButtonProps) {
  const {
    type = "button",
    layout,
    className,
    children,
    disabled,
    onClick,
  } = props;

  const classes = classNames(
    styles.button,
    className,
    disabled && styles.disabled,
    layout && styles[layout],
  );

  return (
    <button onClick={onClick} type={type} className={classes}>
      {children}
    </button>
  );
}
