import { MouseEventHandler, ReactNode } from "react";

export type IButtonProps = {
  type?: "button" | "submit";
  layout?: "outline" | "remove";
  className?: string;
  children?: ReactNode;
  disabled?: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
};
