import { callAddTask } from "@api/tasks";
import { callGetRandomTaskTitle } from "@api/tasksTitle";
import { SWR_KEYS } from "@constants";
import { useCallback, useEffect } from "react";
import { useForm } from "react-hook-form";
import { mutate } from "swr";
import { IAddTaskForm } from "./AddTask.types";

export function useAddTask() {
  const formMethods = useForm<IAddTaskForm>({
    mode: "all",
  });

  const { setValue, reset } = formMethods;

  const setRandomTaskTitle = useCallback(async () => {
    const { data } = await callGetRandomTaskTitle();
    if (data) {
      setValue("title", data.title);
    }
  }, []);

  useEffect(() => {
    setRandomTaskTitle();
  }, []);

  const addTask = useCallback(
    async (variables: IAddTaskForm) => {
      const { data } = await callAddTask(variables);
      if (data) {
        mutate(SWR_KEYS.GET_TASKS);
        setRandomTaskTitle();
      }
    },
    [reset],
  );

  return {
    formMethods,
    addTask,
  };
}
