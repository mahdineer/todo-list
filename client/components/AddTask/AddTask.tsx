import { Button } from "@components/Button";
import { PlusIcon } from "@heroicons/react/outline";
import styles from "./AddTask.module.css";
import { useAddTask } from "./useAddTask";
import { useTranslation } from "react-i18next";

export function AddTask() {
  const { formMethods, addTask } = useAddTask();
  const { handleSubmit, register } = formMethods;
  const { t } = useTranslation("addTask");

  return (
    <form onSubmit={handleSubmit(addTask)} className={styles.addTask}>
      <input
        type="text"
        placeholder={t("texts.writeSomething")}
        {...register("title", { required: true })}
        autoFocus
        autoComplete="off"
      />
      <Button type="submit" className={styles.submitButton}>
        <PlusIcon className={styles.icon} />
      </Button>
    </form>
  );
}
