import { ReactNode } from "react";

export type IModalProps = {
  children: ReactNode;
  isOpen: boolean;
};
