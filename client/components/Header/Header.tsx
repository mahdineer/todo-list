import { AddTask } from "@components/AddTask";
import { Button } from "@components/Button";
import { DeleteModal } from "@components/DeleteModal";
import { Search } from "@components/Search";
import styles from "./Header.module.css";
import { useHeader } from "./useHeader";
import { useTranslation } from "react-i18next";

export function Header() {
  const { isOpen, openModal, deleteAllTask, showAddTask, setShowAddTask } =
    useHeader();
  const { t } = useTranslation(["header", "removeAllTasksModal"]);

  return (
    <>
      <DeleteModal
        isOpen={isOpen}
        title={t("removeAllTasksModal:texts.title")}
        description={t("removeAllTasksModal:texts.description")}
        onDelete={deleteAllTask}
        removeButtonTitle={t("removeAllTasksModal:buttons.removeAll")}
      />
      <div className={styles.header}>
        {showAddTask ? (
          <Button
            onClick={() => setShowAddTask(false)}
            className={styles.addTaskButton}
            type="button"
          >
            {t("header:buttons.search")}
          </Button>
        ) : (
          <Button
            onClick={() => setShowAddTask(true)}
            className={styles.addTaskButton}
            type="button"
          >
            {t("header:buttons.addTask")}
          </Button>
        )}
        <Button
          onClick={openModal}
          className={styles.removeAllButton}
          type="button"
        >
          {t("header:buttons.removeAll")}
        </Button>
      </div>
      {showAddTask ? <AddTask /> : <Search />}
    </>
  );
}
