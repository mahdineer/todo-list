import { SWR_KEYS } from "@constants";
import { callDeleteAllTask } from "@api/tasks";
import { useCallback, useState } from "react";
import { mutate } from "swr";
import { useModalContext } from "@contexts/ModalContext";

export function useHeader() {
  const { isOpen, openModal } = useModalContext();
  const [showAddTask, setShowAddTask] = useState(false);

  const deleteAllTask = useCallback(async () => {
    const { data } = await callDeleteAllTask();
    if (data) {
      mutate(SWR_KEYS.GET_TASKS);
    }
  }, []);

  return {
    isOpen,
    openModal,
    deleteAllTask,
    showAddTask,
    setShowAddTask,
  };
}
