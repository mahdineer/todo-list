import { TasksSkeleton } from "@components/Skeletons/TaskSkeleton";
import { Task } from "@components/Task";
import { ModalProvider } from "@contexts/ModalContext";
import styles from "./Tasks.module.css";
import { ITasksProps } from "./Tasks.types";

export function Tasks(props: ITasksProps) {
  const { tasks, isLoading } = props;

  return isLoading && !tasks?.length ? (
    <TasksSkeleton />
  ) : tasks.length ? (
    <div className={styles.tasks}>
      {tasks.map(task => (
        <ModalProvider key={task.id}>
          <Task className={styles.task} {...task} />
        </ModalProvider>
      ))}
    </div>
  ) : (
    <div className={styles.noTasks}>No tasks yet</div>
  );
}
