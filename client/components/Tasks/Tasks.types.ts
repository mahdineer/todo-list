import { ITask } from "@t/tasks";

export interface ITasksProps {
  tasks: ITask[];
  isLoading?: boolean;
}
