import { ENDPOINTS } from "@constants";
import { callGetTasks } from "@api/tasks";
import { useTasksContext } from "contexts/TasksContext";
import { useEffect } from "react";
import useSWR from "swr";

export function useTasks() {
  const { tasks, setTasks } = useTasksContext();
  const { data } = useSWR(ENDPOINTS.TASKS, callGetTasks);

  useEffect(() => {
    if (data) {
      setTasks(data.data);
    }
  }, [data, setTasks]);

  return {
    tasks,
  };
}
