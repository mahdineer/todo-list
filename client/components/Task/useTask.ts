import { callDeleteTask, callUpdateTask } from "@api/tasks";
import { SWR_KEYS } from "@constants";
import { useModalContext } from "@contexts/ModalContext";
import { IUpdateTask } from "@t/tasks";
import { useCallback } from "react";
import { mutate } from "swr";
import { IUseTaskProps } from "./Task.types";

export function useTask(props: IUseTaskProps) {
  const { id } = props;
  const { isOpen, openModal } = useModalContext();

  const updateTask = useCallback(
    async ({ done }: Pick<IUpdateTask, "done">) => {
      const { data } = await callUpdateTask({ id, done });
      if (data) {
        mutate(SWR_KEYS.GET_TASKS);
      }
    },
    [id],
  );

  const deleteTask = useCallback(async () => {
    const { data } = await callDeleteTask({ id });
    if (data) {
      mutate(SWR_KEYS.GET_TASKS);
    }
  }, [id]);

  return { isOpen, openModal, updateTask, deleteTask };
}
