import classNames from "classnames";
import styles from "./Task.module.css";
import { ITaskProps } from "./Task.types";
import { CheckIcon, TrashIcon } from "@heroicons/react/outline";
import { useTask } from "./useTask";
import { DeleteModal } from "@components/DeleteModal";
import { useTranslation } from "react-i18next";

export function Task(props: ITaskProps) {
  const { id, title, done, className } = props;
  const { isOpen, openModal, updateTask, deleteTask } = useTask({ id });
  const taskClasses = classNames(styles.task, done && styles.done);
  const wrapperClasses = classNames(styles.wrapper, className);
  const { t } = useTranslation("removeTaskModal");
  return (
    <>
      <DeleteModal
        isOpen={isOpen}
        title={t("texts.title")}
        description={t("texts.description")}
        onDelete={deleteTask}
      />
      <div className={wrapperClasses}>
        <div
          onClick={() => updateTask({ done: !done })}
          className={taskClasses}
        >
          <div className={styles.checkbox}>{done && <CheckIcon />}</div>
          <div className={styles.title}>{title}</div>
        </div>
        <button onClick={openModal} className={styles.deleteButton}>
          <TrashIcon className={styles.icon} />
        </button>
      </div>
    </>
  );
}
