import { ITask } from "@t/tasks";

export interface ITaskProps extends ITask {
  className?: string;
}
export interface IUseTaskProps extends Pick<ITask, "id"> {}
