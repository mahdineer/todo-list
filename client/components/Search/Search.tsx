import { SearchIcon } from "@heroicons/react/outline";
import { useTranslation } from "react-i18next";
import styles from "./Search.module.css";
import { useSearch } from "./useSearch";

export function Search() {
  const { formMethods, search } = useSearch();
  const { handleSubmit, register } = formMethods;
  const { t } = useTranslation("common");

  return (
    <form onChange={handleSubmit(search)}>
      <label className={styles.search}>
        <SearchIcon className={styles.icon} />
        <input
          type="text"
          placeholder={t("search")}
          {...register("title")}
          autoFocus
          autoComplete="off"
        />
      </label>
    </form>
  );
}
