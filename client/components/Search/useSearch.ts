import { useTasksContext } from "contexts/TasksContext";
import { useCallback } from "react";
import { useForm } from "react-hook-form";
import { ISearchForm } from "./Search.types";

export function useSearch() {
  const { setSearchTitle } = useTasksContext();
  const formMethods = useForm<ISearchForm>({
    mode: "all",
  });

  const search = useCallback(
    (data: ISearchForm) => {
      const { title } = data;
      setSearchTitle(title);
    },
    [setSearchTitle],
  );

  return {
    formMethods,
    search,
  };
}
