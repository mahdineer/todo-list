import { ENDPOINTS } from "@constants";
import { axiosInstance } from "@api";
import { IGetRandomTaskTitleResponse } from "./@types/taskTitle";

export function callGetRandomTaskTitle() {
  return axiosInstance.get<IGetRandomTaskTitleResponse>(ENDPOINTS.RANDOM_TITLE);
}
