import axios from "axios";

const baseURL = process.env.NEXT_PUBLIC_BASE_URL as string;

const axiosInstance = axios.create({
  baseURL,
});

export { axiosInstance };
