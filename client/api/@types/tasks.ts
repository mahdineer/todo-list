import { ITask } from "@t/tasks";

export interface IAddTaskRequest extends Pick<ITask, "title"> {}

export interface IUpdateTaskRequest extends Omit<ITask, "title"> {}

export interface IDeleteTaskRequest extends Pick<ITask, "id"> {}

export interface IAddTaskResponse extends ITask {}

export interface IGetTasksResponse extends Array<ITask> {}

export interface IUpdateTaskResponse extends ITask {}

export interface IDeleteTaskResponse extends ITask {}

export interface IDeleteAllTaskResponse {
  acknowledged: boolean;
  deletedCount: number;
}
