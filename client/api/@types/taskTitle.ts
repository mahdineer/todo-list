import { ITask } from "@t/tasks";

export interface IGetRandomTaskTitleResponse extends Pick<ITask, "title"> {}
