import { ENDPOINTS } from "@constants";
import { axiosInstance } from "@api";
import {
  IAddTaskRequest,
  IAddTaskResponse,
  IDeleteAllTaskResponse,
  IDeleteTaskRequest,
  IDeleteTaskResponse,
  IGetTasksResponse,
  IUpdateTaskRequest,
  IUpdateTaskResponse,
} from "./@types/tasks";

export function callAddTask(data: IAddTaskRequest) {
  return axiosInstance.post<IAddTaskResponse>(ENDPOINTS.TASKS, data);
}

export function callGetTasks() {
  return axiosInstance.get<IGetTasksResponse>(ENDPOINTS.TASKS);
}

export function callUpdateTask(data: IUpdateTaskRequest) {
  return axiosInstance.put<IUpdateTaskResponse>(ENDPOINTS.TASKS, data);
}

export function callDeleteTask({ id }: IDeleteTaskRequest) {
  return axiosInstance.delete<IDeleteTaskResponse>(`${ENDPOINTS.TASKS}/${id}`);
}

export function callDeleteAllTask() {
  return axiosInstance.delete<IDeleteAllTaskResponse>(ENDPOINTS.TASKS);
}
