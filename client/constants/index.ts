export const ENDPOINTS = {
  TASKS: "/tasks",
  RANDOM_TITLE: "/random-title",
};

export const SWR_KEYS = {
  GET_TASKS: "GET_TASKS",
};
