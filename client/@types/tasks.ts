export interface ITask {
  id: string;
  title: string;
  done: boolean;
}

export interface IUpdateTask {
  id: string;
  done: boolean;
}

export interface IDeleteTask {
  id: string;
}
