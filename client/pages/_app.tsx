import "@styles/globals.css";
import { AppProps } from "next/app";
import "@locales";

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default MyApp;
