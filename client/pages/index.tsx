import { Dashboard } from "@components/Dashboard";
import { TasksProvider } from "@contexts/TasksContext";
import { NextPage } from "next";
import Head from "next/head";

const HomePage: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Todo List</title>
        <meta name="description" content="Simple Todo List Application" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <TasksProvider>
        <Dashboard />
      </TasksProvider>
    </div>
  );
};

export default HomePage;
