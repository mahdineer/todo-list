import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useMemo,
  useState,
} from "react";
import { ITask } from "@t/tasks";

export interface ITasksContext {
  searchTitle: string;
  tasks: ITask[];
}

export type ISetTasksContext = {
  setTasks: Dispatch<SetStateAction<ITasksContext["tasks"]>>;
  setSearchTitle: Dispatch<SetStateAction<ITasksContext["searchTitle"]>>;
};

const initialTasks: ITasksContext = {
  searchTitle: "",
  tasks: [],
};

const TasksContext = createContext<ITasksContext>(initialTasks);
const TasksContextSetState = createContext<ISetTasksContext>({
  setTasks: () => {},
  setSearchTitle: () => {},
});

type Props = {
  children: ReactNode;
};

export function TasksProvider({ children }: Props) {
  const [searchTitle, setSearchTitle] = useState<ITasksContext["searchTitle"]>(
    initialTasks.searchTitle,
  );
  const [tasks, setTasks] = useState<ITasksContext["tasks"]>(
    initialTasks.tasks,
  );

  return (
    <TasksContext.Provider value={{ tasks, searchTitle }}>
      <TasksContextSetState.Provider
        value={{
          setTasks,
          setSearchTitle,
        }}
      >
        {children}
      </TasksContextSetState.Provider>
    </TasksContext.Provider>
  );
}

export function useTasksContext() {
  const { tasks: allTasks, searchTitle } = useContext(TasksContext);
  const { setTasks, setSearchTitle } = useContext(TasksContextSetState);

  const regExp = useMemo(
    () => new RegExp(`(^${searchTitle}.*|\\s${searchTitle}.*)`, "igm"),
    [searchTitle],
  );

  const tasks = useMemo(
    () =>
      searchTitle?.length > 0
        ? allTasks.filter(({ title }) => title.match(regExp))
        : allTasks,
    [allTasks, regExp, searchTitle?.length],
  );

  return { tasks, setTasks, setSearchTitle };
}
