import { createContext, ReactNode, useContext, useState } from "react";
import { Dispatch, SetStateAction } from "react";

export interface IModalContext {
  isOpen: boolean;
}

export interface ISetModalContext {
  setIsOpen: Dispatch<SetStateAction<IModalContext["isOpen"]>>;
}

const initialModal = {
  isOpen: false,
};

const ModalContext = createContext<IModalContext>(initialModal);
const ModalContextSetState = createContext<ISetModalContext>({
  setIsOpen: () => {},
});

type Props = {
  children: ReactNode;
};

export function ModalProvider({ children }: Props) {
  const [isOpen, setIsOpen] = useState<IModalContext["isOpen"]>(
    initialModal.isOpen,
  );

  return (
    <ModalContext.Provider value={{ isOpen }}>
      <ModalContextSetState.Provider
        value={{
          setIsOpen,
        }}
      >
        {children}
      </ModalContextSetState.Provider>
    </ModalContext.Provider>
  );
}

export function useModalContext() {
  const { isOpen } = useContext(ModalContext);
  const { setIsOpen } = useContext(ModalContextSetState);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return { isOpen, setIsOpen, openModal, closeModal };
}
