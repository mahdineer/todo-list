import { resources } from "@locales";

declare module "react-i18next" {
  export interface CustomTypeOptions {
    resources: typeof resources["en"];
  }
}
